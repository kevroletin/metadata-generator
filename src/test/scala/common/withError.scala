package test

import common._

import org.scalatest._
import scala.util.{Try, Success, Failure}

class SimpleSuite extends FunSuite {
  import withError._

  test("1") {
    assert( Ok("Good") + Warning("bad") == Ok("Good", Seq(Warning("bad"))))
  }

  test("2") {
    assert( Ok("Good", Seq(Warning("bad1"))) + Warning("bad2") ==
            Ok("Good", Seq(Warning("bad1"), Warning("bad2")) ) )
  }

  test("3") {
    assert( Fatal("Good") + Warning("bad") == Fatal("Good", Seq(Warning("bad"))))
  }

  test("4") {
    assert( Fatal("Good", Seq(Warning("bad1"))) + Warning("bad2") ==
            Fatal("Good", Seq(Warning("bad1"), Warning("bad2")) ) )
  }
}

class ForExpressionSuite extends FunSuite {
  import withError._
  val a = Ok("val1") + Warning("Bad1")
  val b = Ok("val2") + Warning("Bad2")
  val c = Ok("val3")
  val d = Ok("val4") + Warning("Bad4_1") + Warning("Bad4_2")
  val bad = Fatal("fail") + Warning("BadFail")

  test("for-identity with ok") {
    val res = for { p <- a } yield { p }
    assert( res == a )
  }

  test("for-identity with fail") {
    val res = for { p <- bad } yield { p }
    assert( res == bad )
  }

  test("warning propogation with ok") {
    val res = for {
      pa <- a
      pb <- b
    } yield { pb }
    assert( res == Ok("val2") + Warning("Bad1") + Warning("Bad2") )
  }

  test("warning propogation with fail") {
    val res = for {
      pa <- a
      pb <- bad
    } yield { pb }
    assert( res == ( Fatal("fail")
                     + Warning("Bad1") + Warning("BadFail") ) )
  }

  test("warning propogation with fail(desugared)") {
    val res =
      a.flatMap{ pa =>
        bad.map{ pb =>
          pb
        }
      }
    assert( res == ( Fatal("fail")
                     + Warning("Bad1") + Warning("BadFail") ) )
  }

  test("warning propogation 2 with ok") {
    val res = for {
      pa <- a
      pb <- b
      pc <- c
      pd <- d
    } yield { pb }
    val want = ( Ok("val2")
                 + Warning("Bad1") + Warning("Bad2")
                 + Warning("Bad4_1") + Warning("Bad4_2") )
    assert( res == want )
  }

  test("warning propogation 2 with fail") {
    val res = for {
      pa <- a
      pb <- bad
      pc <- c
      pd <- d
    } yield { pb }
    val want = ( Fatal("fail")
                 + Warning("Bad1") + Warning("BadFail") )
    assert( res == want )
  }
}

class ImplicitConvertionsSuite extends FunSuite {
  import withError._
  import withError.Adaptors._
  import scala.util.{Try, Success, Failure}

  test("use WithError with Try ") {
    val res = for {
      pa <- Ok("good1") + Warning("bad")
      pb <- Try("good2")
    } yield { pb }
    assert( res == Ok("good2") + Warning("bad") )
  }

  test("catch exception using Try") {
    val res = for {
      pa <- Ok("good1") + Warning("bad1")
      pb <- Try{ throw new Exception("bad2"); "good2" }
    } yield { pb }
    assert( res == Fatal("bad2") + Warning("bad1") )
  }
}
