package test
package parsersTest

import org.scalatest._
import scala.util.{Try, Success, Failure}

class SimpleSuite extends FunSuite {
  import common._
  import withError._
  import parsers._

  test("keyword without translation") {
    val content = """|
                     |keyword in english
                     |""".stripMargin
    val res = KeywordsParser.go(content.split('\n').toIterator)
    val ans = Ok(Seq(Keyword("keyword in english", Seq())));
    assert( res == ans )
  }

  test("few keywords without translation") {
    val content = """|
                     |keyword1 in english
                     |
                     |keyword2""".stripMargin
    val res = KeywordsParser.go(content.split('\n').toIterator)
    val ans = Ok(Seq(Keyword("keyword1 in english", Seq()),
                     Keyword("keyword2", Seq())));
    assert( res == ans )
  }

  test("keyword with translation") {
    val content = """|keyword in english
                     |RUS:keyword in russian""".stripMargin
    val res = KeywordsParser.go(content.split('\n').toIterator)
    val ans = Ok(Seq(Keyword("keyword in english",
                             Seq(("RUS", "keyword in russian")))));
    assert( res == ans )
  }

  test("keyword with bad translation") {
    val content = """|keyword in english
                     |keyword in russian
                     |
                     |good keyword""".stripMargin
    val res = KeywordsParser.go(content.split('\n').toIterator)
    val ans = Ok(Seq(Keyword("keyword in english", Seq()),
                     Seq(Warning("bad keyword translation: keyword in russian"))))
    res match {
      case Ok(r, w) => {
        assert(r == List(Keyword("keyword in english"),
                         Keyword("good keyword")))
        assert(w.length == 1)
      }
      case _ => assert( false, "result is not ok" )
    }
  }
}
