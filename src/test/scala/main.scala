package extractor

import common._
import hooks._

import org.scalatest._
import scala.util.{Try, Success, Failure}

object Utils {
  def ensureException(statement: => Unit) = {
    Try(statement) match {
      case Failure(_) =>
      case Success(_) => assert( false, "statement should throw error" )
    }
  }
}

class MixedSuite extends FunSuite {
  import Utils._

  test("check central meredian hook") {
    val mtlKey = "L1_METADATA_FILE.PROJECTION_PARAMETERS.UTM_ZONE"
    def computeRes(zone: Int) = {
      val (res, err) = Hooks.centralMeredianHook(Database(Map(mtlKey -> zone.toString), Map(), Map(), Seq()))
      res.toInt
    }
    assert( computeRes(1) === -177 )
    assert( computeRes(2) === -171 )
    assert( computeRes(60) === 177 )
  }

  test("ckeck bounding box with empty coords list") {
    val coords = List()
    val b = BoundingBox(coords)
    ensureException( b.getPolygon(0) )
    ensureException( b.getPolygon(1) )
  }
}

class BoundingBoxSuite extends FunSuite {
  import Side._
  import Utils._

  test("one polygon (sample data)") {
    val res = "0 1 0 11 10 11 0 11 0 1"
    val a = res.split(" ").grouped(2).map(x => (x(0), x(1))).toList
    val coords = a.map(x => Coord("", x._1, x._2))
    val b = BoundingBox(coords)

    assert( b.getPolygon(0) == res )
    ensureException( b.getPolygon(1) )

    assert( b.getBoundingBox(EastLon) === "10")
    assert( b.getBoundingBox(WestLon) === "0")
    assert( b.getBoundingBox(NorthLat) === "11")
    assert( b.getBoundingBox(SouthLat) === "1")
  }

  test("one polygon and positive/negative x coords(sample data)") {
    val res = "4 2 -2 -4 -4 -2 -2 4 4 2"
    val a = res.split(" ").grouped(2).map(x => (x(0), x(1))).toList
    val coords = a.map(x => Coord("", x._1, x._2))
    val b = BoundingBox(coords)

    assert( b.getPolygon(0) == res )
    ensureException( b.getPolygon(1) )

    assert( b.getBoundingBox(EastLon) === "4")
    assert( b.getBoundingBox(WestLon) === "-4")
    assert( b.getBoundingBox(NorthLat) === "4")
    assert( b.getBoundingBox(SouthLat) === "-4")
  }

  test("date changing meridian(sample data)") {
    val resNeg = "-170 0 -180 0 -180 10 -170 10 -170 0"
    val resPos = "170 0 180 0 180 10 170 10 170 0"
    val a = (resNeg + " " + resPos).split(" ").grouped(2).map(x => (x(0), x(1))).toList
    val coords = a.map(x => Coord("", x._1, x._2))
    val b = BoundingBox(coords)

    assert( b.getPolygon(0) == resPos )
    assert( b.getPolygon(1) == resNeg )
    ensureException( b.getPolygon(2) )

    assert( b.getBoundingBox(WestLon) === "170")
    assert( b.getBoundingBox(EastLon) === "-170")
    assert( b.getBoundingBox(NorthLat) === "10")
    assert( b.getBoundingBox(SouthLat) === "0")
  }

  test("one polygon (live data)") {
    val res = "152.865 69.151 157.208 68.435 159.492 69.999 154.877 70.761 152.865 69.151"
    val a = res.split(" ").grouped(2).map(x => (x(0), x(1))).toList
    val coords = a.map(x => Coord("", x._1, x._2))
    val b = BoundingBox(coords)

    assert( b.getPolygon(0) == res )
    ensureException( b.getPolygon(1) )

    assert( b.getBoundingBox(EastLon) === "159.492")
    assert( b.getBoundingBox(WestLon) === "152.865")
    assert( b.getBoundingBox(NorthLat) === "70.761")
    assert( b.getBoundingBox(SouthLat) === "68.435")
  }

  test("one polygon2 (live data)") {
    val res = "146.528 45.379 147.126 47.093 149.556 46.659 148.886 44.95 146.528 45.379"
    val a = res.split(" ").grouped(2).map(x => (x(0), x(1))).toList
    val coords = a.map(x => Coord("", x._1, x._2))
    val b = BoundingBox(coords)

    assert( b.getPolygon(0) == res )
    ensureException( b.getPolygon(1) )

    assert( b.getBoundingBox(EastLon) === "149.556")
    assert( b.getBoundingBox(WestLon) === "146.528")
    assert( b.getBoundingBox(NorthLat) === "47.093")
    assert( b.getBoundingBox(SouthLat) === "44.95")
  }

  test("one polygon and negative x coords (live data)") {
    val res = "-175.091 63.743 -171.472 63.144 -170.051 64.664 -173.854 65.287 -175.091 63.743"
    val a = res.split(" ").grouped(2).map(x => (x(0), x(1))).toList
    val coords = a.map(x => Coord("", x._1, x._2))
    val b = BoundingBox(coords)

    assert( b.getPolygon(0) == res )
    ensureException( b.getPolygon(1) )

    assert( b.getBoundingBox(EastLon) === "-170.051")
    assert( b.getBoundingBox(WestLon) === "-175.091")
    assert( b.getBoundingBox(NorthLat) === "65.287")
    assert( b.getBoundingBox(SouthLat) === "63.144")
  }

  test("date changing meridian(live data)") {
    val resNeg = "-180 69.34 -176.131 68.704 -178.178 67.118 -180 67.418 -180 69.34"
    val resPos = "180 67.418 177.687 67.799 179.482 69.425 180 69.34 180 67.418"
    val a = (resPos + " " + resNeg).split(" ").grouped(2).map(x => (x(0), x(1))).toList
    val coords = a.map(x => Coord("", x._1, x._2))
    val b = BoundingBox(coords)

    assert( b.getPolygon(0) == resPos )
    assert( b.getPolygon(1) == resNeg )
    ensureException( b.getPolygon(2) )

    assert( b.getBoundingBox(WestLon) === "177.687")
    assert( b.getBoundingBox(EastLon) === "-176.131")
    assert( b.getBoundingBox(NorthLat) === "69.425")
    assert( b.getBoundingBox(SouthLat) === "67.118")
  }
}
