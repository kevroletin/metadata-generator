package withError

import common._
import scala.io._
import scala.util.{Try, Success, Failure}

sealed trait WithError[+T] {
  def isOk: Boolean
  def +(warn: Warning): WithError[T]
  def map[M](f: T => M): WithError[M]
  def flatMap[M](f: T => WithError[M]): WithError[M]
  val warnings: Seq[Warning]
}

object WithError {
  def combine[T](list: List[WithError[T]], default: T): WithError[T] =
    list.foldLeft[WithError[T]](Ok(default)){ (a,b) =>
      for {
        _ <- a
        _ <- b
      } yield default
    }
}

case class Ok[T](res: T, warnings: Seq[Warning] = Seq()) extends WithError[T] {
  def isOk = true
  def +(warn: Warning) = Ok(res, warnings :+ warn)
  def map[M](f: T => M): WithError[M] = Ok(f(res), warnings)
  def flatMap[M](f: T => WithError[M]): WithError[M] = {
    f(res) match {
      case Ok(res, w) => Ok(res, warnings ++ w)
      case Fatal(msg, w) => Fatal(msg, warnings ++ w)
    }
  }
}

case class Fatal[T](msg: String, warnings: Seq[Warning] = Seq()) extends WithError[T] {
  def isOk = false
  def +(w: Warning) = Fatal(msg, warnings :+ w)
  def map[M](f: T => M): WithError[M] = Fatal[M](msg, warnings)
  def flatMap[M](f: T => WithError[M]): WithError[M] = {
    Fatal[M](msg, warnings)
  }
  def toWarning = Warning(s"[error] msg", warnings)
}

case class Warning(msg: String, warnings: Seq[Warning] = Seq()) {
  def :+(w: Warning) = Warning(msg, warnings :+ w)
  def toString(offset: Int): String = {
    val offsetStr: String = (1 to offset).map(x => ' ').mkString("")
    val str = s"${offsetStr}[Warning] $msg"
    if (warnings.isEmpty) str
    else {
      (str +: (warnings.map(_.toString))).mkString("\n")
    }
  }
  override def toString = toString(0)
}

object Adaptors {
  import scala.util.Try._
  import scala.language.implicitConversions

  implicit def try2withError[T](e: Try[T]): WithError[T] = e match {
    case Success(res) => Ok(res)
    case Failure(x: Exception) => Fatal(x.getMessage)
    case Failure(e) => throw(e); Fatal("")
  }
}
