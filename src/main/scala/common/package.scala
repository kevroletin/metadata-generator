package object common {
  object Env {
    val unpackArchives: Boolean = System.getenv("DO_UNPACK") != "false" /* for debug */
  }

  case class Database(
    mtlData: Map[String, String],
    hooks: Map[String, Database => (String, String)],
    coords: Map[String, BoundingBox],
    keywords: Seq[Keyword]
  )

  case class Keyword(en: String, other: Seq[(String, String)] = Seq())

  case class Coord(id: String, x: String, y: String) {
    def isPositive = if (x.length >= 1) x(0) != '-'
    else false
    def getX = x.replace(',', '.')
    def getY = y.replace(',', '.')
    override def toString: String = getX + " " + getY
  }

  object Side extends Enumeration {
    type Side = Value
    val WestLon, EastLon, SouthLat, NorthLat = Value
  }

  case class BoundingBox(polygons: List[String], box: Map[Side.Side, String]) {
    def getPolygon(num: Int): String = polygons(num)
    def getBoundingBox(side: Side.Side): String = box(side)
  }

  object BoundingBox {
    private def mkPolygons(coords: List[Coord]): List[String] = {
      if (coords.isEmpty) List()
      else if (coords.length <= 5) List(coords.map(_.toString).mkString(" "))
      else {
        val a: Map[Boolean, List[Coord]] = coords.groupBy(_.isPositive)
        List(true, false).filter(a.isDefinedAt(_)).map { x =>
          a(x).map(_.toString).mkString(" ")
        }
      }
    }

    private def mkBox(coords: List[Coord]): Map[Side.Side, String] = {
      import Side._
      if (coords.isEmpty) Map()
      else {
        if (coords.length <= 5) {
          Map((EastLon -> coords.maxBy(_.getX.toDouble).getX),
            (WestLon -> coords.minBy(_.getX.toDouble).getX),
            (NorthLat -> coords.maxBy(_.getY.toDouble).getY),
            (SouthLat -> coords.minBy(_.getY.toDouble).getY))
        } else {
          val a: Map[Boolean, List[Coord]] = coords.groupBy(_.isPositive)
          val pos = a(true)
          val neg = a(false)
          Map((WestLon -> pos.minBy(_.getX.toDouble).getX),
            (EastLon -> neg.maxBy(_.getX.toDouble).getX),
            (NorthLat -> coords.maxBy(_.getY.toDouble).getY),
            (SouthLat -> coords.minBy(_.getY.toDouble).getY))
        }
      }
    }

    def apply(coords: List[Coord]): BoundingBox = {
      new BoundingBox( mkPolygons(coords), mkBox(coords) )
    }
  }
  
}
