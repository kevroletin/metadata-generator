package parsers

import common._
import scala.io._
import withError._

object TemplateParser {

  protected def transformLine(str: String, data: Database): WithError[String] = {
    val pattern = "\\$\\{([^}]+)\\}".r
    var errors: List[String] = List()
    def findMtlValue(key: String, data: Database): String = {
      if ( data.mtlData.contains( key ) ) {
        data.mtlData(key)
      } else if ( data.hooks.contains( key ) ) {
        val (res, err) = data.hooks(key)(data)
        if ( !err.isEmpty ) {
          errors = err :: errors
        }
        res
      } else {
        errors = s"$key not found in mtl file" :: errors
        s"###$key###"
      }
    }
    val res = pattern replaceAllIn( str, m => findMtlValue(m.group(1), data) )
    Ok(res, errors.map(x => Warning(x)))
  }

  def apply(it: List[String], data: Database ): WithError[List[String]] = {
    val res = it.map{ transformLine(_, data) }.toList
    res.foldRight[WithError[List[String]]](Ok(List())){ (a, res) =>
      for {
        pa <- a
        pres <- res
      } yield pa +: pres
    }
  }
}
