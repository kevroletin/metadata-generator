package parsers

import common._
import scala.io._
import withError._

object CoordsFileParser {
  object CoordPattern {
    def req(str: String) = s"($str)"
    val fid = "\\d+"
    val shape = "Point ZM"
    val objectId = "\\d+"
    val entityId = "[\\d\\w]+"
    val origRid = "\\d+"
    val pointX = "\\-?\\d+,?\\d+"
    val pointY = pointX
    val space = "\\s+"
    val pattern = List(req(entityId), origRid, req(pointX), req(pointY)).mkString(space).r
    def unapply(str: String): Option[(String, String, String)] = {
      str match {
        case pattern(id, x, y) => Some(id, x, y)
        case _ => None
      }
    }
  }

  protected def go(coordsFile: Iterator[String]): WithError[Map[String, BoundingBox]] = {
    var coords: Map[String, List[Coord]] = Map().withDefaultValue(List())
    var res: WithError[Unit] = Ok{}
    var line = 0
    coordsFile.foreach{ str =>
      line += 1
      str match {
        case CoordPattern(id, x, y) => {
          val oldVal = coords(id)
          coords += (id -> (oldVal :+ Coord( id, x, y )))
        }
        case str =>
          if (line > 1) res = res + Warning(s"[error] bad string: $str")
      }
    }
    res.map{ x => coords.mapValues(x => BoundingBox(x)) }
  }

  def apply(coordsFile: String): WithError[Map[String, BoundingBox]] = {
    logger.info(s"Reading files with coordinates $coordsFile")
    var res = go( Source.fromFile(coordsFile, "utf-8").getLines )
    res.map { m =>
      if (m.size == 0) res = res + Warning(s"$coordsFile is empty")
      m
    }
  }
}
