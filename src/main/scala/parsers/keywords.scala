package parsers

import common._
import withError._
import scala.io._

object KeywordsParser {

  def isComment(str: String): Boolean = {
    val pat = "#.*".r
    str match {
      case pat() => true
      case _ => false
    }
  }

  def eatTranslations(it: Iterator[String],
                      res: Seq[(String, String)] = Seq(),
                      warn: Seq[Warning] = Seq())
                      : (Seq[(String, String)], Seq[Warning]) =
    if (!it.hasNext) (res, warn)
    else {
      val pattern = "(\\w+):(.*)".r
      val empty = "\\s*".r
      it.next.trim match {
        case empty() => (res, warn)
        case pattern(k, v) => {
          eatTranslations(it, res :+ (k, v), warn)
        }
        case str => {
          val err = Warning(s"bad keyword translation: $str")
          eatTranslations(it, res, warn :+ err)
        }
      }
    }

  def eatOne(it: Iterator[String]): WithError[Seq[Keyword]] =
    if (!it.hasNext) Ok(Seq())
    else {
      val line = it.next.trim
      if (line.isEmpty) eatOne(it)
      else {
        val en = line;
        val (other, warn) = eatTranslations(it)
        Ok(Seq(Keyword(en, other)), warn)
      }
    }

  def go(input: Iterator[String]): WithError[Seq[Keyword]] = {
    val it = input.filter(x => !isComment(x))
    var res: WithError[Seq[Keyword]] = eatOne(it)
    while (it.hasNext) {
      res = for {
        a <- res
        b <- eatOne(it)
      } yield {
        a ++ b
      }
    }
    res
  }

  def apply(tagsFile: String): WithError[Seq[Keyword]] = {
    go( Source.fromFile(tagsFile, "utf-8").getLines )
  }
}
