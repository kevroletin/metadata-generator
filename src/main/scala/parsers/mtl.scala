package parsers

import scala.io._
import withError._

/* Simple parser based on regular expression
 * Potential problems:
 *  - requires each key - values pair to be on separate line
 *  - doesn't handle special symbols inside strings e.g. \t
 */
object MtlParser {

  def notEmpty(it: Iterator[String]): Boolean =
    it.exists{x: String => notEmpty(x)}

  def notEmpty(s: String): Boolean = {
    val empty = "\\s*".r
    s match {
      case empty() => false
      case _ => true
    }
  }

  def remQuites(s: String): String = {
    val pattern = "\"(.*)\"".r
    s.trim match {
      case pattern(res) => res
      case res => res
    }
  }

  def doParse(it: Iterator[String],
              prefixes: List[String] = List(),
              res     : WithError[Map[String, String]] = Ok(Map()))
              : WithError[Map[String, String]] =
    if ( !it.hasNext ) res
    else {
      val pattern = "\\s*(\\S+)\\s*=\\s*(.+)".r
      val str = it.next()
      str match {
        case pattern(key, value_) => {
          val value = remQuites( value_ )
          if (key == "GROUP") {
            doParse(it, value :: prefixes, res)
          } else if (key == "END_GROUP") {
            val newRes =
              if (value == prefixes.head) res
              else res + Warning("END_GROUP for " + value + ". Expected " + prefixes.head)
            doParse(it, prefixes.tail, newRes)
          } else {
            val fullPath = (key :: prefixes).reverse.mkString(".")
            val newRes = res.map( m => m + (fullPath -> value) )
            doParse(it, prefixes, newRes)
          }
        }
        case "END" => {
          if (!notEmpty(it)) res
          else res + Warning("There are data after END")
        } 
        case _ => {
          doParse(it, prefixes, res + Warning("Error: bad string " + str + "\n"))
        }
      }
    }
  
  def apply(mtlFile: String): WithError[Map[String, String]] =
    doParse(Source.fromFile(mtlFile, "utf-8").getLines)
}
