package fileutils

import common._
import fileutils.imagescaler.ImageScaler
import scala.io._
import java.io._
import scala.sys.process._
import scala.util.{Try, Success, Failure}

import common._
import withError._

object FileUtils {
  val envVar = "CYGWIN" -> "nodosfilewarning"

  def checkEnv: Try[Unit] = Try {
    val checkers = List(Seq("tar", "--help"), Seq("unzip", "--help"));
    for ( cmd <- checkers ) {
      def fail = throw new Exception(s"${cmd(0)} is not installed")
      val outGrabber = ProcessLogger(_ => (), err => fail)
      Try[Int]( cmd ! outGrabber ) match {
        case Success(0) => ()
        case _ => fail
      }
    }
  }

  def untar(file: String, destDir: String): WithError[Unit] =
    if ( !Env.unpackArchives ) Ok{}
    else {
      logger.info( s"Extracting $file" )
      val cmd = Seq("tar", "xfv", file, "-C", destDir)
      val res = Process(cmd, None, envVar) ! ProcessLogger(x => logger.info(x), x => logger.error(x))
      if ( 0 == res ) Ok{}
      else Fatal(s"can't extract $file, exit code $res")
    }

  def unzip(file: String, destDir: String): WithError[Unit] =
    if ( !Env.unpackArchives ) Ok{}
    else {
      logger.info( s"Extracting $file" )
      val cmd = Seq("unzip", "-o", "-d", destDir, file)
      val res = Process(cmd, None, envVar) ! ProcessLogger(x => logger.info(x), x => logger.error(x))
      if ( 0 == res ) Ok{}
      else Fatal(s"can't extract $file, exit code $res")
    }

  def copyJpg(fileName: String, destName: String, reqHeight: Int): WithError[Unit] = {
    if ( reqHeight == 0 ) {
      logger.info("Skip image resize")
      copyFile(fileName, destName)
    } else {
      ImageScaler(fileName, destName, reqHeight) match {
        case Success(_) => Ok{}
        case Failure(e: Exception) => Fatal(s"Can't move and resize $fileName: ${e.getMessage}")
        case Failure(x) => throw(x); Fatal("")
      }
    }
  }

  private def execShell(cmd: Seq[String], errMsg: String): WithError[Unit] = {
    val res = cmd ! ProcessLogger(x => logger.info(x), x => logger.error(x))
    if ( 0 == res ) Ok{}
    else Fatal(errMsg + s"; exit code $res")
  }

  def copyFile(fileName: String, destName: String): WithError[Unit] = {
    logger.info(s"copy $fileName to $destName")
    execShell( Seq("cp", fileName, destName), s"can't move $fileName -> $destName" )
  }
  val copyPng = copyFile _

  def deleteDirectory(dirName: String): WithError[Unit] = {
    logger.info( s"Deleting $dirName" )
    execShell( Seq("rm", "-rf", dirName), s"can't delete $dirName" )
  }

  def archiveDir(workingDir: String, dirName: String, archiveName: String): WithError[Unit] = {
    logger.info( s"Archiving $dirName to $archiveName from $workingDir" )
    val cmd = Process( Seq("zip", "-r", archiveName, dirName), new File(workingDir) )
    val res = cmd ! ProcessLogger(x => logger.info(x), x => logger.error(x))
    if ( 0 == res ) Ok{}
    else Fatal(s"can't archive $dirName; exit code $res")
  }
}
