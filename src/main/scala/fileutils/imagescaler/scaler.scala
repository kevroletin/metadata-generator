package fileutils.imagescaler

import scala.util.{Try, Success, Failure}
import java.awt.Graphics2D
import java.awt.image.BufferedImage
import java.io.IOException
import javax.imageio.ImageIO
import java.awt.AlphaComposite
import java.awt.RenderingHints
import java.io._

object ImageScaler {
  private def resizeImage(originalImage: BufferedImage, width: Int, height: Int): BufferedImage = {
    val t: Int = if (originalImage.getType() == 0) BufferedImage.TYPE_INT_ARGB
    else originalImage.getType()
    val resizedImage: BufferedImage = new BufferedImage(width, height, t)
    val g: Graphics2D = resizedImage.createGraphics()

    g.setComposite(AlphaComposite.Src)
    g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR)
    g.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY)
    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)

    g.drawImage(originalImage, 0, 0, width, height, null)
    g.dispose();

    resizedImage
  }

  def apply(fileName: String, destName: String, reqHeight: Int) = Try {
    val file = new File(fileName)
    val img: BufferedImage = ImageIO.read(file)
    val (w, h) = (img.getWidth, img.getHeight)
    val (nw, nh) = (w/(h / reqHeight), reqHeight)
    logger.info(s"copy $fileName to $destName and resize from ($w x $h) to ($nw x $nh)")
    val resultImg: BufferedImage = resizeImage(img, nw, nh)
    ImageIO.write(resultImg, "png", new File(destName))
  }
}
