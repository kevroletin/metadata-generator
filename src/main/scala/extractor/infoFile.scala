package extractor

import java.io._
import java.util.Date
import withError._

object InfoTemplate {
  val d = new Date
  val dateStr = String.format("%tFT%tT", d, d)
  val header = s"""<?xml version="1.0" encoding="UTF-8"?>
<info version="1.1">
  <general>
    <createDate>${dateStr}</createDate>
    <changeDate>${dateStr}</changeDate>
    <schema>iso19139</schema>
    <isTemplate>false</isTemplate>
    <format>full</format>
    <uuid>ReplaceMeWithUUID</uuid>
    <siteId>Dummy</siteId>
    <siteName>PGI Metadata Catalogue</siteName>
  </general>
  <categories>
    <category name="datasets" />
  </categories>
  <privileges>
    <group name="all">
      <operation name="view" />
    </group>
  </privileges>
  <public>
""";

  val footer = """</public>
  <private />
</info>
""";

  def fileTemplate(filename: String) =
    s"""<file name="$filename" changeDate="$dateStr" />"""

  def apply(outFile: String, images: Seq[String]): WithError[Unit] = {
    val fileList = images.map(fileTemplate(_)).mkString("\n")
    val out = new PrintWriter( outFile, "UTF-8")
    out.println(header)
    out.println(fileList)
    out.println(footer)
    out.close
    Ok{}
  }
}
