package extractor

import parsers._
import common._
import hooks._
import fileutils._
import withError._

import scala.io._
import java.io._
import scala.util.matching.Regex.Match
import scala.util.{Try, Success, Failure}
import scala.sys.process._

object DirectoryWallker {
  import withError.Adaptors._

  case class Job(name: String, archives: List[String])

  val thumbnailNames = Seq("_QL.png", "_QL_s.png")

  def fillTemplate(outFile: String,
                   mtlFile: String,
                   xmlTemplate: List[String],
                   coords: Map[String, BoundingBox],
                   keywords: Seq[Keyword])
                   : WithError[Unit] =
    for {
      mtlData <- MtlParser( mtlFile )
      content <- TemplateParser( xmlTemplate, Database(mtlData, Hooks.hooks, coords, keywords) )
      _ <- Try {
        val out = new PrintWriter( outFile, "UTF-8")
        content.foreach( out.println(_) )
        out.close
      }
    } yield ()

  def nameWithoutDirAndExtention(name: String): String = {
    val dirs = name.split("[\\\\/]")
    if (dirs.isEmpty) name
    else {
      val parts = dirs.last.split("\\.")
      if (parts.size < 2) name
      else parts.init.mkString(".")
    }
  }

  def getFilesByExtention(dir: File, ext: String): List[File] = {
    dir.listFiles.filter(_.getName.toLowerCase.endsWith(ext)).toList
  }

  def getFilesByExtention(dir: String, ext: String): List[File] =
    getFilesByExtention(new File(dir), ext)

  def unpackArchives(fileNames: List[String], outDirName: String): WithError[Unit] = {
    val res: List[WithError[Unit]] = for {
      x <- fileNames
    } yield {
      if (x.endsWith("zip")) {
        FileUtils.unzip(x, outDirName)
      } else {
        FileUtils.untar(x, outDirName)
      }
    }
    WithError.combine(res, ())
  }

  def copyImages(workingDirName: String, outDirName: String, imgHeight: Seq[Int]): WithError[List[String]] = {
    logger.info(s"copying files from ${workingDirName} to ${outDirName}")
    var images: List[String] = List()
    val res = for {
      jpg <- getFilesByExtention(workingDirName, ".jpg").toList
      if ( !jpg.getName.toLowerCase.endsWith("tir.jpg") )
    } yield {
      if (imgHeight.isEmpty) {
        images = List(s"${outDirName}/${thumbnailNames(0)}")
        FileUtils.copyFile(jpg.toString, s"${outDirName}/${thumbnailNames(0)}")
      } else {
        val jpgRes = for { (h, ext) <- imgHeight.toList zip thumbnailNames } yield {
          val imgName = nameWithoutDirAndExtention(jpg.toString) + ext
          images = images :+ imgName
          FileUtils.copyJpg(jpg.toString, s"${outDirName}/$imgName", h)
        }
        WithError.combine(jpgRes, ())
      }
    }
    for { _ <- WithError.combine(res, ()) } yield images
  }

  def findJobs(dir: String): Seq[Job] = {
    val res = findArchives(new File(dir))
    res.groupBy(_._1).mapValues(_.map(_._2))
        .toList.map(x => Job(x._1, x._2)).sortWith(_.name < _.name)
  }

  def findArchives(dir: File): List[(String, String)] = {
    val dirs = dir.listFiles.filter(_.isDirectory)
    val archives: List[(String, String)] = for {
      x <- List(".zip", ".tar.gz")
      y <- dir.listFiles.filter(_.getName.toLowerCase.endsWith(x))
    } yield {
      (y.getName.dropRight(x.length), y.toString)
    }
    val subRes = dirs.flatMap(findArchives(_))
    archives ++ subRes
  }

  def ensureDirExists(dir: String, create: Boolean): Try[Unit] = {
    val file = new File(dir)
    if (file.exists || (create && file.mkdirs)) Success()
    else Try( throw new Exception(s"$dir directory doesn't exists") )
  }

  def reportWarnings[T](value: WithError[T]): Try[T] = {
    val (res, w) = value match {
      case Ok(data, w) => (Success(data), w)
      case Fatal(msg, w) => (Try{ throw new Exception(msg) }, w)
    }
    for (x <- w) logger.info(s"[warn] $x")
    res
  }

  def populateKeywords(jobs: Seq[Job], cfg: Config) = {
    for ( x <- jobs )  yield {
      val pat = """LC\d(\d\d\d)(\d\d\d)[\d\w]+""".r
      val (path, row) = (x.name: @unchecked) match {
        case pat(path, row) => (path, row)
      }
      val file = s"${cfg.keywordsDir}/${path}_$row.keys.txt"
      if ( new File(file).exists ) {
        logger.info(s"found keywords for ${path}_$row", true)
      } else {
        logger.info(s"created new keywords file for ${path}_$row", true)
        val out = new PrintWriter(file, "UTF-8")
        out.println(s"""|# This is keywords for path:$path row:$row
                        |# -------------------------------------
                        |# Please remove this line and add keywords""".stripMargin)
        out.close()
        if (!cfg.textEditor.isEmpty) Seq(cfg.textEditor, file) .!
      }
      var res = KeywordsParser(file)
      var ok: Boolean = !cfg.textEditor.isEmpty
      while (!res.warnings.isEmpty && ok) {
        val content = Source.fromFile(file, "UTF-8").getLines.toList
        val out = new PrintWriter(file, "UTF-8")
        for (w <- res.warnings) out.println(s"# $w");
        content.foreach( out.println(_) )
        out.close();

        ok = (Seq(cfg.textEditor, file) .!) == 0
        res = KeywordsParser(file)
      }
      res
    }
  }

  def go(cfg: Config): Try[Unit] =
    for {
      _ <- FileUtils.checkEnv
      _ <- ensureDirExists( cfg.inDir, false )
      _ <- ensureDirExists( cfg.outDir, true )
      _ <- ensureDirExists( cfg.tmpDir, true )
      _ <- ensureDirExists( cfg.keywordsDir, true )
      coords <- reportWarnings( CoordsFileParser(cfg.coordsFile) )
      xmlTemplate <- Try{ logger.info(s"Reading template file ${cfg.xmlTemplate}")
                          Source.fromFile(cfg.xmlTemplate, "utf-8").getLines.toList }
    } yield {
      val jobs     = findJobs(cfg.inDir)
      val keywords = populateKeywords(jobs, cfg)
      for {
        ((Job(name, archives), keys), i) <- (jobs zip keywords).zipWithIndex
      } yield {
        logger.info(s" * Processing $name [${i+1}/${jobs.size}] *", true)
        val workDir           = s"${cfg.tmpDir}/$name"
        val outDir            = s"${cfg.outDir}/$name"
        val imageDir          = s"$outDir/public"
        val privateDir        = s"$outDir/private"
        val metadataDir       = s"$outDir/metadata"
        val resultingMetadata = s"${metadataDir}/metadata.xml"
        new File(imageDir).mkdirs
        new File(privateDir).mkdirs
        new File(metadataDir).mkdirs
        new File(workDir).mkdirs

        val res = for {
          k <- keys
          _ <- unpackArchives(archives, workDir)
          images <- copyImages(workDir, imageDir, cfg.imageHeight)
          _ <- fillTemplate(resultingMetadata,
                            s"${workDir}/${name}_MTL.txt",
                            xmlTemplate, coords, k)
          _ <- InfoTemplate(s"$outDir/info.xml", images)
        } yield ()

        reportWarnings(res)
        res match {
          case Ok(_, w) => {
            if (w.length == 0) {
              logger.info("   done without problems", true)
            } else {
              logger.info(s"   ! done with warnings (see log for details)", true)
            }
          }
          case Fatal(msg, _) => logger.error(s"   $msg", true)
        }        
      }
      if (cfg.deleteTmp) { FileUtils.deleteDirectory(cfg.tmpDir) }
      logger.info("Create MEF archive")
      FileUtils.archiveDir(cfg.outDir, ".", "batch.mef")
      ()
    }


  def checkKeywords(cfg: Config) = {
    for {
      file <- getFilesByExtention( cfg.keywordsDir, "txt" )
    } yield {
      val fname = file.toString
      val res = KeywordsParser(fname)
      for (err <- res.warnings) {
        logger.error( s"$fname - $err" )
      }
      res.map { fileKeywords =>
        for (k <- fileKeywords) {
          if (k.other.isEmpty) {
            logger.error( s"$fname - translations are missed" )
          }
          else if ( k.other.find(_._1 == "ENG").isEmpty ) {
            logger.error( s"$fname - ENG translation is missed" )
          }
        }
      }
    }
  }

}

case class Config(
  inDir: String = "In",
  outDir: String = "Out",
  tmpDir: String = "Tmp",
  keywordsDir: String = "Keywords",
  textEditor: String = "notepad",
  coordsFile: String = "coords.txt",
  xmlTemplate: String = "template.xml",
  deleteTmp: Boolean = false,
  imageHeight: List[Int] = List(800, 176),
  logFile: String = "",
  checkKeywords: Boolean = false
)

object Main {
  val parser = new scopt.OptionParser[Config]("scopt") {
    opt[String]('i', "input") action { (x, c) =>
      c.copy(inDir = x) } text("directory with input data")
    opt[String]('o', "output") action { (x, c) =>
      c.copy(outDir = x) } text("directory with output data")
    opt[String]('t', "temporary") action { case (x, c) =>
      c.copy(tmpDir = x) } text("temporary directory")
    opt[String]('x', "template") action { case (x, c) =>
      c.copy(xmlTemplate = x) } text("xml template")
    opt[String]('c', "coords") action { case (x, c) =>
      c.copy(coordsFile = x) } text("csv file with coordinates")
    opt[String]('k', "keywords") action { case (x, c) =>
      c.copy(keywordsDir = x) } text("directory with keywords database")
    opt[String]('e', "editor") action { case (x, c) =>
      c.copy(textEditor = x) } text("command which runs text editor")
    opt[Boolean]('d', "delete-tmp") action { case (x, c) =>
      c.copy(deleteTmp = x) } text("remove unpacked data after work")
    opt[String]('l', "log") action { case (x, c) =>
      c.copy(logFile = x) } text("write log into file instead of stdout")
    opt[String]("img-height") action { case (x, c) =>
      val res = x.split(",").map(_.toInt).toList
      c.copy(imageHeight = res) } text("comma separated list of image heights (empty - dont resize)")
    opt[Boolean]("check-keywords") action { case (x, c) =>
      c.copy(checkKeywords = x) } text("check keywords database")
    help("help") text("prints this usage text")
  }

  def main(args: Array[String]): Unit =
    parser.parse(args, Config()) map { config =>
      if ( !config.logFile.isEmpty ) {
        logger.openFile("log.txt")
      }
      logger.info(config.toString)

      if (config.checkKeywords)
        DirectoryWallker.checkKeywords(config)
      else {
        val err = DirectoryWallker.go(config)
          (err: @unchecked) match {
          case Success(_) => println("Done")
          case Failure(e) => println(e)
        }
      }
      logger.closeFile
    }
}

