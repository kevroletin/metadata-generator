package hooks

import java.util.Date
import common._
import scala.util.{Try, Success, Failure}

object Hooks {
  def tryToStr(res: Try[String]): (String, String)  = {
    (res: @unchecked) match {
      case Success(str) => (str, "")
      case Failure(e: Exception) => ("###Error###", e.getMessage)
    }
  }

  def get(d: Database, x: String) = Try( d.mtlData(x) )

  def dateTimeHook(d: Database): (String, String) = {
    val res = for {
      data     <- get(d, "L1_METADATA_FILE.PRODUCT_METADATA.DATE_ACQUIRED")
      timeFull <- get(d, "L1_METADATA_FILE.PRODUCT_METADATA.SCENE_CENTER_TIME")
      time     <- Try( (timeFull split "\\.")(0) )
    } yield {
      s"${data}T${time}"
    }
    tryToStr(res)
  }

  def dateHook(d: Database): (String, String) = {
    val res = for {
      data <- get(d, "L1_METADATA_FILE.PRODUCT_METADATA.DATE_ACQUIRED")
    } yield {
      s"${data}"
    }
    tryToStr(res)
  }

  def dateYearHook(d: Database): (String, String) = {
    val res = for {
      data <- get(d, "L1_METADATA_FILE.PRODUCT_METADATA.DATE_ACQUIRED")
    } yield {
      data.split("-").head
    }
    tryToStr(res)
  }

  def fileDateTimeHook(d: Database): (String, String) = {
    val d = new Date
    (String.format("%tFT%tT", d, d), "")
  }

  def titleHook(language: String)(d: Database): (String, String) = {
    val res = for {
      path <- get(d, "L1_METADATA_FILE.PRODUCT_METADATA.WRS_PATH")
      row  <- get(d, "L1_METADATA_FILE.PRODUCT_METADATA.WRS_ROW")
      spacecraft <- get(d, "L1_METADATA_FILE.PRODUCT_METADATA.SPACECRAFT_ID")
      scene <- get(d, "L1_METADATA_FILE.METADATA_FILE_INFO.LANDSAT_SCENE_ID")
    } yield {
      if ( "ru" == language ) {
        s"$spacecraft - Виток: $path Ряд: $row Сцена: $scene"
      } else {
        s"$spacecraft - Path: $path Row: $row for Scene $scene"
      }
    }
    tryToStr(res)
  }

  def alternateTitleHook(d: Database): (String, String) = {
    val res = for {
      sensor     <- get(d, "L1_METADATA_FILE.PRODUCT_METADATA.SENSOR_ID")
      spacecraft <- get(d, "L1_METADATA_FILE.PRODUCT_METADATA.SPACECRAFT_ID")
    } yield {
      spacecraft + sensor.replace("_", "/")
    }
    tryToStr(res)
  }

  def centralMeredianHook(d: Database): (String, String) = {
    val res = for {
      zone    <- get(d, "L1_METADATA_FILE.PROJECTION_PARAMETERS.UTM_ZONE")
      zoneInt <- Try(zone.toInt)
    } yield {
      (( 3 + (zoneInt - 1) * 6 ) - 180).toString
    }
    tryToStr(res)
  }

  def zoneHook(padding: Boolean)(d: Database): (String, String) = {
    val res = for {
      zone    <- get(d, "L1_METADATA_FILE.PROJECTION_PARAMETERS.UTM_ZONE")
      zoneInt <- Try(zone.toInt)
    } yield {
      if ( padding ) {
        f"$zoneInt%02d"
      } else {
        s"$zoneInt"
      }
    }
    tryToStr(res)
  }

  def poligonHook(num: Int)(d: Database): (String, String) = {
    val res = for {
      scene  <- get(d, "L1_METADATA_FILE.METADATA_FILE_INFO.LANDSAT_SCENE_ID")
      coords <- Try( d.coords(scene).getPolygon(num) )
    } yield {
      if (num == 0) {
        coords
      } else {
        s"""<gml:interior>
                  <gml:LinearRing>
                    <gml:posList>$coords</gml:posList>
                  </gml:LinearRing>
                </gml:interior>"""
      }
    }
    if (num == 0) tryToStr(res)
    else {
      res match {
        case Success(str) => (str, "")
        case _ => ("", "")
      }
    }
  }

  def boundingBoxHook(side: Side.Side)(d: Database): (String, String) = {
    val res = for {
      scene  <- get(d, "L1_METADATA_FILE.METADATA_FILE_INFO.LANDSAT_SCENE_ID")
      coords <- Try( d.coords(scene).getBoundingBox(side) )
    } yield {
      coords
    }
    tryToStr(res)
  }

  def wrsRowHook(field: String)(d: Database): (String, String) = {
    tryToStr( for {
      raw <- get(d, "L1_METADATA_FILE.PRODUCT_METADATA." + field)
      rawInt <- Try( raw.toInt )
    } yield {
      f"$rawInt%03d"
    })
  }

  def keywordsHook(d: Database): (String, String) = {
    val head = """|      <gmd:descriptiveKeywords>
                  |        <gmd:MD_Keywords>
                  |""".stripMargin
    val footer =
      """|
         |          <gmd:type>
         |            <gmd:MD_KeywordTypeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_KeywordTypeCode" codeListValue="place" />
         |          </gmd:type>
         |        </gmd:MD_Keywords>
         |      </gmd:descriptiveKeywords>""".stripMargin
    def translation(d: (String, String)) = {
      val (lang, data) = d
      (  """                """
       + s"""<gmd:LocalisedCharacterString locale="${lang}">"""
       + data
       + """</gmd:LocalisedCharacterString>""")
    }
    def translations(list: Seq[(String, String)]) =
      list.map(translation(_)).mkString("")
    def keyword(key: Keyword) = {
      val transText = translations(key.other)
      val trans = s"""|            <gmd:PT_FreeText>
                      |              <gmd:textGroup>
                      |${transText} 
                      |              </gmd:textGroup>
                      |            </gmd:PT_FreeText>""".stripMargin

      s"""|          <gmd:keyword xsi:type="gmd:PT_FreeText_PropertyType">
          |            <gco:CharacterString>${key.en}</gco:CharacterString>
          |${trans}
          |          </gmd:keyword>""".stripMargin

    }
    val res = ( head
              + d.keywords.map(keyword(_)).mkString("\n")
              + footer )
    (res, "")
  }

  val hooks: Map[String, Database => (String, String)] =
    Map( ("&dateTime" -> dateTimeHook),
         ("&date"     -> dateHook),
         ("&dateYear" -> dateYearHook),
         ("&fileDateTime" -> fileDateTimeHook),
         ("&titleEn"  -> titleHook("en")),
         ("&titleRu"  -> titleHook("ru")),
         ("&alternateTitle"  -> alternateTitleHook),
         ("&centralMeredian" -> centralMeredianHook),
         ("&zone"       -> zoneHook(false)),
         ("&zonePadded" -> zoneHook(true)),
         ("&poligon"    -> poligonHook(0)),
         ("&poligonSnd" -> poligonHook(1)),
         ("&boundinbBoxWest" -> boundingBoxHook(Side.WestLon)),
         ("&boundinbBoxEast" -> boundingBoxHook(Side.EastLon)),
         ("&boundinbBoxSouth" -> boundingBoxHook(Side.SouthLat)),
         ("&boundinbBoxNorth" -> boundingBoxHook(Side.NorthLat)),
         ("&wrsRow"  -> wrsRowHook("WRS_ROW")),
         ("&wrsPath" -> wrsRowHook("WRS_PATH")),
         ("&placeKeywords" -> keywordsHook) )
}
