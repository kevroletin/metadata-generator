Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Сахалинская область
ENG:Sakhalin Oblast

о. Сахалин
ENG:Sakhalin I.

залив Байкал
ENG:gulf Baikal

залив Астох
ENG:gulf Astokh

залив Чайво
ENG:gulf Chayvo

Татарский пролив
ENG:Strait of Tartary

Музьма
ENG:Muz’ma

Колендо
ENG:Kolendo

Оха
ENG:Okha

Эхаби
ENG:Ekhabi

Восточный
ENG:Vostochnyy

Тунгор
ENG:Tungor

Некрасовка
ENG:Nekrasovka

Москальво
ENG:Moskal’vo

Люги
ENG:Lyugi

Луполово
ENG:Lupolovo

Нов. Лангры
ENG:Novye Langry

Береговые Лангры
ENG:Beregovye Langry

Большереченск
ENG:Bol’sherechensk

Сабо
ENG:Sabo

Нефтегорск
ENG:Neftegorsk

Пильтун
ENG:Pil’tun

Горомай
ENG:Goromay

Чайво
ENG:Chayvo

Вал
ENG:Val

Охотское море
ENG:Sea of Okhotsk

