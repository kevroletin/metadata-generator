Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Хабаровский край
ENG:Khabarovsk Krai

Сахалинская область
ENG:Sakhalin Oblast

о. Сахалин
ENG:Sakhalin I.

залив Байкал
ENG:gulf Baikal

Татарский пролив
ENG:Strait of Tartary

Петровская Коса
ENG:Petrovskaya Kosa

Власьево
ENG:Vlas’evo

Оха
ENG:Okha

Эхаби
ENG:Ekhabi

Восточный
ENG:Vostochnyy

Тунгор
ENG:Tungor

Некрасовка
ENG:Nekrasovka

Москальво
ENG:Moskal’vo

Люги
ENG:Lyugi

Луполово
ENG:Lupolovo

Нов. Лангры
ENG:Novye Langry

Береговые Лангры
ENG:Beregovye Langry

Большереченск
ENG:Bol’sherechensk

Сабо
ENG:Sabo

Нефтегорск
ENG:Neftegorsk

Байдуково
ENG:Baydukovo

Меньшиково
ENG:Men’shikovo

Макаровка
ENG:Makarovka

Пуир
ENG:Puir

Новопокровка
ENG:Novopokrovka

Свободное
ENG:Svobodnoye

Оремиф
ENG:Oremif

Тнейвах
ENG:Tneyvakh

Джаоре
ENG:Dzhaore

Алеевка
ENG:Aleyevka

Николаевск на Амуре
ENG:Nikolayevsk na Amure

Красное
ENG:Krasnoye

Константиновка
ENG:Konstantinovka

Иннокентьевка
ENG:Innokent’evka

Денисовка
ENG:Denisovka

р. Амур
ENG:Amur River

Gulf of Sakhalin
ENG:Сахалинский залив

Охотское море
ENG:Sea of Okhotsk

