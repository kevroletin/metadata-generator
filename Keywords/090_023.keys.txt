Поверхность Земли
ENG:Land Surface

Северная Америка
ENG:NORTH AMERICA

Соединённые Штаты Америки
ENG:United States of America

США
ENG:USA

Аляска
ENG:Alaska

Алеутские острова
ENG:Aleutian Islands

о. Атту
ENG:Attu I.

о. Агатту
ENG:Agattu I.

Берингово море
ENG:Bering Sea

Тихий океан
ENG:Pacific Ocean

