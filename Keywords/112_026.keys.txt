Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Хабаровский край
ENG:Khabarovsk Krai

Приморский край
ENG:Primorsky Krai

Горный
ENG:Gornyy

Кия
ENG:Kiya

Бира
ENG:Bira

Верхняя Манома
ENG:Verkhnyaya Manoma

Манома 1-я
ENG:Manoma 1

Анюй
ENG:Anyuy

Пихца
ENG:Pikhtsa

Эморон
ENG:Emoron

Троицкое
ENG:Troitskoye

Найхин
ENG:Naykhin

Даерга
ENG:Daerga

Гасси
ENG:Gassi

Мыс Маяк
ENG:Mys Mayak

Сарапульское
ENG:Sarapul’skoye

Елабуга
ENG:Elabuga

Вятское
ENG:Vyatskoye

Малышево
ENG:Malyshevo

Князе-Волконское
ENG:Knyaze-Volkonskoye

Черная Речка
ENG:Chernaya Rechka

Дружба
ENG:Druzhba

Некрасовка
ENG:Nekrasovka

р. Амур
ENG:Amur River

р. Сумми
ENG:Summi River

р. Немта
ENG:Nemta River

р. Анюй
ENG:Anyuy River

р. Хор
ENG:Khor River

