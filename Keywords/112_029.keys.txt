Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Приморский край
ENG:Primorsky Krai

Малиново
ENG:Malinovo

Ариадное
ENG:Ariadnoye

Пожига
ENG:Pozhiga

Дальний
ENG:Dal’niy

Пластун
ENG:Plastun

Черемшаны
ENG:Cheremshany

Гранитный
ENG:Granitnyy

Дальнегорск
ENG:Dal’negorsk

Садовое
ENG:Sadovoye

Сержантово
ENG:Serzhantovo

Лидовка
ENG:Lidovka

Каменка
ENG:Kamenka

Рудная Пристань
ENG:Rudnaya Pristan’

Мономахово
ENG:Monomakhovo

Зеркальный
ENG:Zerkal’nyy

Богополь
ENG:Bogopol’

Суворово
ENG:Suvorovo

Краснореченский
ENG:Krasnorechenskiy

Окраинка
ENG:Okrainka

Самарка
ENG:Samarka

Хрустальный
ENG:Khrustal’nyy

Рудный
ENG:Rudnyy

Фабричный
ENG:Fabrichnyy

Кавалерово
ENG:Kavalerovo

Горнореченский
ENG:Gornorechenskiy

Шумный
ENG:Shumnyy

Уборка
ENG:Uborka

Кокшаровка
ENG:Koksharovka

Чугуевка
ENG:Chuguevka

Соколовка
ENG:Sokolovka

Булыга-Фадеево
ENG:Bulyga-Fadeyevo

Ленино
ENG:Lenino

Фурманово
ENG:Furmanovo

Веселый Яр
ENG:Veselyy Yar

Ракушка
ENG:Rakushka

Тимофеевка
ENG:Timofeyevka

Нордост
ENG:Nordost

Серафимовка
ENG:Serafimovka

р. Бол. Уссурка
ENG:Bol’shaya Ussurka River

р. Малиновка
ENG:Malinovka River

р. Уссури
ENG:Ussuri River

р. Зеркальная
ENG:Zerkal’naya River

Японское море
ENG:Sea of Japan

