Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Магаданская область
ENG:Magadan Oblast

Дукат
ENG:Dukat

Сеймчан
ENG:Seymchan

Верх. Сеймчан
ENG:Verkhniy Seymchan

Колымское
ENG:Kolymskoye

Усть-Среднекан
ENG:Ust-Srednekan

Буюнда
ENG:Buyunda

Суксукан
ENG:Suksukan

Солнечный
ENG:Solneсhny

Лунный
ENG:Lunny

р. Колыма
ENG:Kolyma River

р. Сугой
ENG:Sugoy River

р. Буюнда
ENG:Buyunda River

р. Балыгычан
ENG:Balygychan River

