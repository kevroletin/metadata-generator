Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Хабаровский край
ENG:Khabarovsk Krai

Агние-Афанасьевск
ENG:Agnie-Afanas’evsk

Ягодный
ENG:Yagodnyy

Черный мыс
ENG:Chernyy Mys

Новоильиновка
ENG:Novoil’inovka

Киселевка
ENG:Kiselevka

им. Максима Горького
ENG:Im. Maksima Gor’kogo

Ключевой
ENG:Klyuchevoy

Сухановка
ENG:Sukhanovka

Циммермановка
ENG:Tsymmermanovka

Быстринск
ENG:Bystrinsk

Решающий
ENG:Reshayushchiy

Де-Кастри
ENG:De-Kastri

Булава
ENG:Bulava

Мариинское
ENG:Mariinskoye

Мариинский Рейд
ENG:Mariinskiy Reyd

Софийск
ENG:Sofiysk

Койма
ENG:Koyma

Савинское
ENG:Savinskoye

Монгол
ENG:Mongol

Дуди
ENG:Dudi

Кольчем
ENG:Kol’chem

Солонцы
ENG:Solontsy

Тулинское
ENG:Tulinskoye

Чильба
ENG:Chil’ba

Кизи
ENG:Kizi

Бол. Санники
ENG:Bol’shye Sanniki

Калиновка
ENG:Kalinovka

р. Амур
ENG:Amur River

оз. Удыль
ENG:Lake Udyl’

оз. Дудинское
ENG:Lake Dudinskoye

оз. Кади
ENG:Lake Kadi

оз. Бол. Кизи
ENG:Lake Bol’shye Kizi

оз. Мал. Кизи
ENG:Lake Malye Kizi

