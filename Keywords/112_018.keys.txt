Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Хабаровский край
ENG:Khabarovsk Krai

Кетанда
ENG:Ketanda

Арка
ENG:Arka

Симга
ENG:Simga

Чумка
ENG:Chumka

Ключи
ENG:Klyuchi

Утунур
ENG:Utunur

Медвежка
ENG:Medvezhka

Дуран
ENG:Duran

Кухтуй
ENG:Kykhtuy

Громовская
ENG:Gromovskaya

Немкин
ENG:Nemkin

Золотой
ENG:Zolotoy

Бакирка
ENG:Bakirka

Резиденция
ENG:Rezidentsiya

Кирпичный
ENG:Kirpichnyy

Партизанский
ENG:Partizanskiy

Кавказ
ENG:Kavkaz

Охотск
ENG:Okhotsk

Булгин
ENG:Bulgin

Морской
ENG:Morskoy

Новое Устье
ENG:Novoye Ust’e

Аэропорт
ENG:Aeroport

Конская
ENG:Konskaya

Тырай
ENG:Tyray

Новая Земля
ENG:Novaya Zemlya

Вострецово
ENG:Vostretsovo

Опыт
ENG:Opyt

Урак
ENG:Urak

р. Ульбея
ENG:Ul’beya River

р. Кухтуй
ENG:Kukhtuy River

р. Охота
ENG:Okhota River

р. Урак
ENG:Urak River

Охотское море
ENG:Sea of Okhotsk

