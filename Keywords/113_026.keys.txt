Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Китайская Народная Республика
ENG:Peoples Republic of China

Российская Федерация
ENG:Russian Federation

Хабаровский край
ENG:Khabarovsk Krai

р. Уссури
ENG:Ussiru River

р. Амур
ENG:Amur River

р. Кур
ENG:Kur River

р. Симми
ENG:Simmi River

р. Немта
ENG:Nemta River

Хабаровск
ENG:Khabarovsk

Троицкое
ENG:Troitskoye

Мухен
ENG:Mukhen

Кофовский
ENG:Korfovskiy

Березовка
ENG:Berezovka

им. Тельмана
ENG:Im. Tel’mana

Приамурский
ENG:Priamurskiy

Николаевка
ENG:Nikolaevka

Смидович
ENG:Smidovich

Волочаевка 2-я
ENG:Volochaevka 2

Волочаевка 1-я
ENG:Volochaevka 1

Новокуровка
ENG:Novokurovka

Мал. Сидима
ENG:Malaya Sidima

Дурмин
ENG:Durmin

Обор
ENG:Obor

Сита
ENG:Sita

Некрасовка
ENG:Nekrasovka

Дружба
ENG:Druzhba

Ильинка
ENG:Il’inka

Ракитное
ENG:Rakitnoye

Гаровка
ENG:Garovka

Черная Речка
ENG:Chernaya Rechka

Князе-Волконское
ENG:Knyaze-Volkonskoye

Матвеевка
ENG:Matveyevka

Малышево
ENG:Malyshevo

Елабуга
ENG:Elabuga

Сарапульское
ENG:Sarapul’skoye

Мыс-Маяк
ENG:Mys-Mayak

Даерга
ENG:Daerga

Найхин
ENG:Naykhin

Троицкое
ENG:Troitskoye

