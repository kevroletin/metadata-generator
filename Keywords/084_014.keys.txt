Поверхность Земли
ENG:Land Surface

Северная Америка
ENG:NORTH AMERICA

Азия
ENG:ASIA

Соединённые Штаты Америки
ENG:United States of America

США
ENG:USA

Аляска
ENG:Alaska

Российская Федерация
ENG:Russian Federation

Чукотский автономный округ
ENG:Chukotka Autonomous Okrug

Чукотское море
ENG:Chukchi Sea

Берингов пролив
ENG:Bering Strait

мыс Дежнева
ENG:Cape Dezhnev

Уэлен
ENG:Uelen

Лаврентия
ENG:Lavrentiya

о. Ратманова
ENG:Big Diomede I.

