Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Хабаровский край
ENG:Khabarovsk Krai

Агние-Афанасьевск
ENG:Agnie-Afanas’evsk

Чукчагир
ENG:Chukchagir

Черный мыс
ENG:Chernyy Mys

Новоильиновка
ENG:Novoil’inovka

Киселевка
ENG:Kiselevka

им. Максима Горького
ENG:Im. Maksima Gor’kogo

Ключевой
ENG:Klyuchevoy

Сухановка
ENG:Sukhanovka

Восток 2-й
ENG:Vostok 2

Малютка
ENG:Malyutka

Пакто
ENG:Pakto

Джалкан
ENG:Dzhalkan

Малома
ENG:Maloma

Харпичан
ENG:Kharpichan

Кондон
ENG:Kondon

Синдан
ENG:Sindan

Нган
ENG:Ngan

Горин
ENG:Gorin

Пиль
ENG:Pil’

Мавринский
ENG:Mavrinskiy

Боктор
ENG:Boktor

Шелехово
ENG:Shelekhovo

Нижнетамбовское
ENG:Nizhnetambovskoye

Опытное Поле
ENG:Opytnoye Pole

р. Амур
ENG:Amur River

р. Бичи
ENG:Bichi River

р. Амгунь
ENG:Amgun’ River

р. Горюн
ENG:Goryun River

оз. Чукчагирское
ENG:Lake Chukchagirskoye

оз. Эворон
ENG:Lake Evoron

