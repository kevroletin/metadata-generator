Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Приморский край
ENG:Primorsky Krai

Хабаровский край
ENG:Khabarovsk Krai

Сологцовый
ENG:Solontsovyy

Катэн
ENG:Katen

Ходы
ENG:Khody

Гвасюги
ENG:Gvasyugi

Южный
ENG:Yuzhnyy

Меца
ENG:Metsa

Долми
ENG:Dolmi

Медвежий
ENG:Medvezhyy

Шумный
ENG:Shumnyy

Золотой
ENG:Zolotoy

Хоменгу
ENG:Khomengu

Мал. Сидима
ENG:Malaya Sidima

Мухен
ENG:Mikhen

Дурмин
ENG:Durmin

Обор
ENG:Obor

Сита
ENG:Sita

Бичевая
ENG:Bichevaya

Полетное
ENG:Poletnoye

Георгиевка
ENG:Georgievka

Екатериновка
ENG:Ekaterinovka

Капитоновка
ENG:Kapitonovka

р. Сукпай
ENG:Sukpay River

р. Хор
ENG:Khor River

