Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Республика Саха (Якутия)
ENG:Sakha (Yakutia) Republic

р. Алдан
ENG:Aldan River

р. Лена
ENG:Lena River

Кангалассы
ENG:Kangalassy

Жатай
ENG:Zhatay

Тулагино
ENG:Tulagino

Огородтах
ENG:Ogorodtakh

Кысыл-Сыр
ENG:Kysyl-Syr

Крест-Кытып
ENG:Krest-Kytyp

Намцы
ENG:Namtsy

Кытыл
ENG:Kytyl

Уо-Кюеля
ENG:Uo-Kyuelya

Богородицы
ENG:Bogoroditsy

Хотоннох
ENG:Khotonnokh

Хатты
ENG:Khatty

Кептени
ENG:Kepteni

Дюпся
ENG:Dyupsya

Тюнгюлю
ENG:Tyungyulyu

Кюелерики
ENG:Kyueleriki

