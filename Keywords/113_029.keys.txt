Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Китайская Народная Республика
ENG:Peoples Republic of China

Российская Федерация
ENG:Russian Federation

Приморский край
ENG:Primorsky Krai

Горные Ключи
ENG:Gornye Klyuchi

Кировский
ENG:Kirovsky

Арсеньев
ENG:Arsenyev

Сибирцево
ENG:Sibirtsevo

Реттиховка
ENG:Rettikhovka

Спасск
ENG:Spassk

Черниговка
ENG:Chernigovka

Кокшаровка
ENG:Koksharovka

Яковлевка
ENG:Yakovlevka

Кремово
ENG:Kremovo

Ляличи
ENG:Lyalichi

Монастырище
ENG:Monastyrishche

Дмитриевка
ENG:Dmitrievka

Красный Кут
ENG:Krasnyy Kut

Прохоры
ENG:Prokhory

Хвалынка
ENG:Khvalynka

Новосельское
ENG:Novosel’skoye

Александровка
ENG:Aleksandrovka

Свиягино
ENG:Sviyagino

Чкаловское
ENG:Chkalovskoye

Павло-Федоровка
ENG:Pavlo-Fedorovka

Тихменево
ENG:Tichmenevo

Самарка
ENG:Samarka

Шумный
ENG:Shumnyy

Уборка
ENG:Uborka

Булыга-Фадеево
ENG:Bulyga-Fadeyevo

Соколовка
ENG:Sokolovka

Яковлевка
ENG:Yakovlevka

Варфоломеевка
ENG:Varfolomeyevka

Новосысоевка
ENG:Novosysoevka

Чернышевка
ENG:Chernyshevka

Анучино
ENG:Anuchino

р. Уссури
ENG:Ussuri River

р. Илистая
ENG:Ilistaya River

р. Арсеньевка
ENG:Arsen’evka River

р. Журавлевка
ENG:Zhuravlevka River

р. Павловка
ENG:Pavlovra River

оз. Ханка
ENG:Lake Khanka

