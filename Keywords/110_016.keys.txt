Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Магаданская область
ENG:Magadan Oblast

Большевик
ENG:Bol’shevik

Нексикан
ENG:Neksikan

Холодный
ENG:Kholodnyy

Сусуман
ENG:Susuman

Мяутджа
ENG:Myaundzha

Кадыкчан
ENG:Kadykchan

Адыгалах
ENG:Adygalakh

Усть-Хакчан
ENG:Ust-Khakchan

Бурхала
ENG:Burkhala

Буркандья
ENG:Burkand’ya

Эсчан
ENG:Eschan

Широкий
ENG:Shiroky

Ударник
ENG:Udarnik

Мальдяк
ENG:Mal’dyak

Орротук
ENG:Orotuk

р. Аян-Юрях
ENG:Ayan-Yuryakh River

р. Кулу
ENG:Kulu River

