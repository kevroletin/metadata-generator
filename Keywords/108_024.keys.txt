Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Сахалинская область
ENG:Sakhalin Oblast

о. Сахалин
ENG:Sakhalin I.

Вал
ENG:Val

Ноглики
ENG:Nogliki

Катангли
ENG:Katangli

Гаромай
ENG:Garomay

Чайво
ENG:Chayvo

Даги
ENG:Dagi

Венское
ENG:Venskoye

Горячие Ключи
ENG:Goryachiye Klyuchi

Набиль
ENG:Nabil’

Арги-Паги
ENG:Argi-Pagi

р. Тымь
ENG:Tym’ River

залив Чайво
ENG:gulf Chayvo

Ныйский залив
ENG:Nyysky gulf

Набильский залив
ENG:Nabilsky gulf

Охотское море
ENG:Sea of Okhotsk

