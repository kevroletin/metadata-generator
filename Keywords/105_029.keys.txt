Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Япония
ENG:Japan

п-ов Сиретоко
ENG:Siretoko Peninsula

Российская Федерация
ENG:Russian Federation

Сахалинская область
ENG:Sakhalin Oblast

Тихий океан
ENG:Pacific Ocean

Охотское море
ENG:Sea of Okhotsk

о. Итуруп
ENG:Iturup I.

Курильские острова
ENG:Kurile Islands

о. Кунашир
ENG:Kunashir I.

Малая Курильская гряда
ENG:Lesser Kuril Ridge

о. Шикотан
ENG:Shikotan I.

пр. Кунаширский (Немуро)
ENG:Kunashirsky Strait (Nemuro)

пр. Екатерины
ENG:Catherine Strait

Южно-Курильский пролив
ENG:South Kuril Strait

Южно-Курильск
ENG:Yuzhno-Kuril'sk

Тятиво
ENG:Tyatino

