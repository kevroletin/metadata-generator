Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Республика Саха (Якутия)
ENG:Sakha (Yakutia) Republic

Восточно-Сибирское море
ENG:East Siberian Sea

пр. Дмитрия Лаптева
ENG:Dmitry Laptev's strait

Юкагир
ENG:Yukagir

о. Макар
ENG:Makar I.

о. Вост. Шелонский
ENG:Vostochnyy Shelonsky I.

Шелонские острова
ENG:Shelonsky islands

Эбелляхская губа
ENG:Ebelyakhsky lip

Van’kina lip
ENG:Ванькина губа

Селляхская губа
ENG:Sellyakhsky lip

Янский залив
ENG:Yana Bay

