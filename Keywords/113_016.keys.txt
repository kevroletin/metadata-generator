Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Республика Саха (Якутия)
ENG:Sakha (Yakutia) Republic

Томтор
ENG:Tomtor

Аян-Кюеле
ENG:Ayan-Kyuele

Аэропорт
ENG:Aeroport

Балаган
ENG:Balagan

Переправа
ENG:Pereprava

Бюгюех
ENG:Byugyuekh

Орто-Балаган
ENG:Orto-Balagan

Хастах
ENG:Khastakh

Куранах-Сала
ENG:Kuranakh-Sala

Челаха
ENG:Chelakha

Береа-Юрдя
ENG:Berea-Yurdya

Оймякон
ENG:Oymyakon

Хара-Тумул
ENG:Khara-Tumul

Учугей
ENG:Uchyugey

Агаякан
ENG:Agayakan

р. Кюенте
ENG:Kyuente River

р. Индигирка
ENG:Indigirka River

