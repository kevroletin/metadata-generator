Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Республика Саха (Якутия)
ENG:Sakha (Yakutia) Republic

Восточно-Сибирское море
ENG:East Siberian Sea

море Лаптевых
ENG:Laptev Sea

Новосибирские острова
ENG:New Siberian Archipelago

Ляховские острова
ENG:Lyakhov Islands

о. Бол. Ляховский
ENG:Bol’shoy Lyakhovskiy I.

о. Мал. Ляховский
ENG:Malyy Lyakhovskiy I.

пр. Санникова
ENG:Sannikov strait

пр. Этерикан
ENG:Eterikan strait

пр. Дмитрия Лаптева
ENG:Dmitry Laptev's strait

Эбелляхская губа
ENG:Ebelyakhsky lip

