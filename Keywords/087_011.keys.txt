Поверхность Земли
ENG:Land Surface

Северная Америка
ENG:NORTH AMERICA

Соединённые Штаты Америки
ENG:United States of America

США
ENG:USA

Аляска
ENG:Alaska

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Чукотский автономный округ
ENG:Chukotka Autonomous Okrug

Чукотское море
ENG:Chukchi Sea

Северный ледовитый океан
ENG:Arctic Ocean

