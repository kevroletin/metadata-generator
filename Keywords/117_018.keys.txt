Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Республика Саха (Якутия)
ENG:Sakha (Yakutia) Republic

Хабаровский край
ENG:Khabarovsk Krai

Эльдикан
ENG:El’dikan

Усть-Мая
ENG:Ust-Maya

Новый Чай
ENG:Novyy Chay

Кельбик
ENG:Kel’bik

Эжанцы
ENG:Ezhantsy

Петропавловск
ENG:Petropavlovsk

Троицк
ENG:Troitsk

Аппа
ENG:Appa

Усть-Миль
ENG:Ust-Mil’

р. Алдан
ENG:Aldan River

р. Аллах-Юнь
ENG:Allakh-Yun’ River

р. Мая
ENG:Maya River

