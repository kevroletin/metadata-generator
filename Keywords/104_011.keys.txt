Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Чукотский автономный округ
ENG:Chukotka Autonomous Okrug

Республика Саха (Якутия)
ENG:Sakha (Yakutia) Republic

Северный ледовитый океан
ENG:Arctic Ocean

Восточно-Сибирское море
ENG:East Siberian Sea

Колымский залив
ENG:Kolyma gulf

Медвежка
ENG:Medvezhka

Ручьишки
ENG:Ruch’ishki

Яндрино
ENG:Yandrino

Малая Бараниха
ENG:Malaya Baranikha

Крестовое
ENG:Krestovoye

Лель
ENG:Lel’

р. Колыма
ENG:Kolyma River

