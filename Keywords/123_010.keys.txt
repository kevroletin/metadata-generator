Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Республика Саха (Якутия)
ENG:Sakha (Yakutia) Republic

море Лаптевых
ENG:Laptev Sea

Янский залив
ENG:Yana Bay

о. Ярок
ENG:Yarok I.

Нижнеянск
ENG:Nizhneyansk

Северный
ENG:Severnyy

Кулар
ENG:Kular

Власово
ENG:Vlasovo

Энтузиастов
ENG:Entuziastov

Хайыр
ENG:Khayyr

Казачье
ENG:Kazach’e

Усть-Янск
ENG:Ust-Yansk

р. Яна
ENG:Yana River

р. Чондон
ENG:Chondon River

р. Омолой
ENG:Omoloy River

