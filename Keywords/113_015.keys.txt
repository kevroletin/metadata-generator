Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Магаданская область
ENG:Magadan Oblast

Республика Саха (Якутия)
ENG:Sakha (Yakutia) Republic

Арга-Мой
ENG:Arga-Moy

Хатынгнах
ENG:Khatyngnakh

Предпорожный
ENG:Predporozhnyy

Артык
ENG:Artyk

Ольчан
ENG:Ol’chan

Усть-Нера
ENG:Ust-Nera

Нелькан
ENG:Nel’kan

Дорожный
ENG:Dorozhnyy

Победа
ENG:Pobeda

Бурустах
ENG:Burustakh

Дражный
ENG:Drazhnyy

Богатырь
ENG:Bogatyr’

р. Индигирка
ENG:Indigirka River

р. Нера
ENG:Nera River

