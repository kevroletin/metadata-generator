Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Камчатский край
ENG:Kamchatka Krai

Сахалинская область
ENG:Sakhalin Oblast

п-ов Камчатка
ENG:Kamchatka Peninsula

м. Лопатка
ENG:Cape Lopatka

Курильские острова
ENG:Kurile Islands

о. Шумшу
ENG:Shumshu I.

о. Парамушир
ENG:Paramushir I.

о. Атласова
ENG:Atlasov I.

о. Анциферова
ENG:Antsyferov I.

о. Маканруши
ENG:Makanrushi I.

Первый Курильский пролив
ENG:First Kuril strait

Второй Курильский пролив
ENG:Second Kuril strait

Четвертый Курильский пролив
ENG:Fourth Kuril strait

пр. Лужина
ENG:Luzhin strait

Охотское море
ENG:Sea of Okhotsk

Тихий океан
ENG:Pacific Ocean

Северо-Курильск
ENG:Severo-Kuril’sk

Семеновка
ENG:Semenovka

Курбатово
ENG:Kurbatovo

Бабушкино
ENG:Babushkino

Шумный
ENG:Shumny

Шелихово
ENG:Shelikhovo

