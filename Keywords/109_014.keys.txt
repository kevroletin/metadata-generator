Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Магаданская область
ENG:Magadan Oblast

Республика Саха (Якутия)
ENG:Sakha (Yakutia) Republic

Глухариный
ENG:Glukhariny

Ороек
ENG:Oroek

Нелемное
ENG:Nelemnoye

Оттур-Кюель
ENG:Ottur-Kyuel’

Зырянка
ENG:Zyryanka

Верхнеколымск
ENG:Verkhnekolymsk

Мангазейка
ENG:Mangazeyka

Усун-Кюель
ENG:Ysyn-Kyuel’

Угольное
ENG:Ugol’noye

р. Колыма
ENG:Kolyma Ruver

р. Ожогина
ENG:Ozhogina River

р. Ясачная
ENG:Yasachnaya River

р. Поповка
ENG:Popovka River

