Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Хабаровский край
ENG:Khabarovsk Krai

Амурская область
ENG:Amur Oblast

Завитинск
ENG:Zavitinsk

Райчихинск
ENG:Raychikhinsk

Новорайчихинск
ENG:Novoraychikhinsk

Новобурейский
ENG:Novo-Bureyskiy

Бурея
ENG:Bureya

Прогресс
ENG:Progress

Архара
ENG:Arkhara

Поярково
ENG:Poyarkovo

Ромны
ENG:Romny

Козьмодемьяновка
ENG:Koz’modem’yanovka

Екатеринославка
ENG:Ekaterinoslavka

р. Бурея
ENG:Bureya River

р. Архара
ENG:Arkhara River

