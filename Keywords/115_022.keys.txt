Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Хабаровский край
ENG:Khabarovsk Krai

Удское
ENG:Udskoye

Аимни
ENG:Aimni

Тором
ENG:Torom

Мыс Тыльский
ENG:Mys Tyl’skiy

Неран
ENG:Neran

Алгазея
ENG:Algazeya

Чумикан
ENG:Chumikan

р. Уда
ENG:Uda River

р. Мая
ENG:Maya River

р. Тором
ENG:Torom River

Уддская губа
ENG:Uddsky lip

Шантарское море
ENG:Shantarsky sea

Охотское море
ENG:Sea of Okhotsk

