Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Республика Саха (Якутия)
ENG:Sakha (Yakutia) Republic

Томмок
ENG:Tommot

Лебединый
ENG:Lebedinyy

Ярокут
ENG:Yarokut

Ленинский
ENG:Leninskiy

Алдан
ENG:Aldan

Ниж. Куранах
ENG:Nizhniy Kuranakh

Большой Нимныр
ENG:Bol’shoy Nimnyr

Алексеевск
ENG:Alekseyevsk

Заречный
ENG:Zarechnyy

Ханпарастах
ENG:Khanparastakh

Угоян
ENG:Ugoyan

Усть-Селигдар
ENG:Ust-Seligdar

Yakokit
ENG:Якокит

Хытыстыр
ENG:Khytystyr

Тобук
ENG:Tobuk

Перекатный
ENG:Perekatnyy

Суон-Тит
ENG:Suon-Tit

р. Алдан
ENG:Aldan River

р. Амга
ENG:Amga River

