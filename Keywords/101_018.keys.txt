Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Russian Federation
ENG:Российская Федерация

Kamchatka Krai
ENG:Камчатский край

Kamchatka Peninsula
ENG:п-ов Камчатка

Magadan Oblast
ENG:Магаданская область

Lesnaya
ENG:Лесная

Taygonos Peninsula
ENG:п-ов Тайгонос

Cape Taygonos
ENG:м. Тайгонос

Penzhina Bay
ENG:Пенжинская губа

Shelikhov Bay
ENG:залив Шелихова

Sea of Okhotsk
ENG:Охотское море

