Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Сахалинская область
ENG:Sakhalin Oblast

Хабаровский край
ENG:Khabarovsk Krai

о. Сахалин
ENG:Sakhalin I.

Ильинский
ENG:Il’insky

Пензенское
ENG:Penzenskoye

Томари
ENG:Tomari

Черемшанка
ENG:Cheremshanka

Неводское
ENG:Nevodskoye

Урожайное
ENG:Urozhaynoye

Новоселово
ENG:Novoselovo

Новосибирское
ENG:Novosibirskoye

Красноярское
ENG:Krasnoyarskoye

Чехов
ENG:Chekhov

Татарский пролив
ENG:Strait of Tartary

Охотское море
ENG:Sea of Okhotsk

Японское море
ENG:Sea of Japan

