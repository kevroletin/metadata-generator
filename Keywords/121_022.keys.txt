Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Амурская область
ENG:Amur Oblast

Тында
ENG:Tynda

Восточный
ENG:Vostochnyy

Соловьевск
ENG:Solov’evsk

Сковородино
ENG:Skovorodino

Невер
ENG:Never

Уркан
ENG:Urkan

Золотая Гора
ENG:Zolotaya Gora

Яныр
ENG:Yanyr

Лев. Тында
ENG:Levaya Tynda

Островной
ENG:Ostrovnoy

Джуваскит
ENG:Dzhuvaskit

Новый Благовещенский
ENG:Novyy Blagodtshchenskiy

Могоктак
ENG:Mogoktak

Беленькая
ENG:Belen’kaya

Джелтулак
ENG:Dzheltulak

Кировский
ENG:Kirovskiy

р. Гилюй
ENG:Gilyuy River

р. Уркан
ENG:Urkan River

