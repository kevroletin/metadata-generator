Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Республика Саха (Якутия)
ENG:Sakha (Yakutia) Republic

Северный ледовитый океан
ENG:Arctic Ocean

Восточно-Сибирское море
ENG:East Siberian Sea

Колымский залив
ENG:Kolyma gulf

Михалкино
ENG:Mikhalkino

Походск
ENG:Pokhodsk

Горла
ENG:Gorla

Чукочья
ENG:Chukoch’ya

Становая
ENG:Stanovaya

Стадухино
ENG:Stadukhino

Галявино
ENG:Galyavino

р. Колыма
ENG:Kolyma River

