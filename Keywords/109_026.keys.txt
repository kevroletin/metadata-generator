Поверхность Земли
ENG:Land Surface

Азия
ENG:ASIA

Российская Федерация
ENG:Russian Federation

Сахалинская область
ENG:Sakhalin Oblast

Хабаровский край
ENG:Khabarovsk Krai

о. Сахалин
ENG:Sakhalin I.

Монгохто
ENG:Mongokhto

Ванино
ENG:Vanino

Октябрьский
ENG:Oktyabr’skiy

Заветы Ильича
ENG:Zvety Il’icha

Майский
ENG:Mayskiy

Лососина
ENG:Lososina

Советская Гавань
ENG:Sovetskaya Gavan’

Гатка
ENG:Gatka

Иннокентьевский
ENG:Innokent’evskiy

Лесогорск
ENG:Lesogorsk

Тельновский
ENG:Tel’novskiy

Шахтерск
ENG:Shakhtersk

Ударный
ENG:Udarnyy

Углегорск
ENG:Uglegorsk

Красногорск
ENG:Krasnogorsk

Краснополье
ENG:Krasnopol’e

Айнское
ENG:Aynskoye

Изыльметьево
ENG:Izyl’met’evo

Татарский пролив
ENG:Strait of Tartary

Охотское море
ENG:Sea of Okhotsk

