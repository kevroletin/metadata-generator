import AssemblyKeys._

assemblySettings

jarName in assembly := "MetadataExtractor.jar"

name := "MetadataExtractor"

version := "0.1"

scalacOptions ++= Seq("-deprecation", "-feature")

libraryDependencies ++= Seq(
  "com.github.scopt" %% "scopt" % "3.2.0",
  "org.scalacheck" %% "scalacheck" % "1.10.1" % "test",
  "org.scalatest" %% "scalatest" % "2.0" % "test"
)

resolvers += Resolver.sonatypeRepo("public")

TaskKey[Unit]("cleanOut") := {
  import scala.sys.process._
  "rm -rf ./Out".!
  "rm -rf ./Tmp".!
}
